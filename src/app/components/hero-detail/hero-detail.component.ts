import { Component, OnInit, Input } from '@angular/core';
import { Hero } from "../../models/hero";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { HeroService } from 'src/app/services/hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})

export class HeroDetailComponent implements OnInit {

  @Input() hero: Hero;
  
  constructor(
    private route: ActivatedRoute,
    private service: HeroService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.service.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  goBack(){
    this.location.back();
  }
}
